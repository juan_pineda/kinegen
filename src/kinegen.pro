;+
;
; INPUTS:
; file		: a fits or ad2 file containing the velocity field
;
; CALL:
; kinegen,file,params,error_file=error_file,snr_file=snr_file,$
;	plotall=plotall,plotlast=plotlast,verbose=verbose,$
;	name=name,seed=seed,ctrl=ctrl,threading=threading,seeing=seeing,$
;	rwd_index=rwd_index,d0=d0,cover=cover
;
; Last modified:
; V.Perret March 2012
;
;
; NAME:
;	KINEGEN
;
; PURPOSE:
;	This procedure fits the velocity field parameters [PA,incl,xcenter,ycenter] with the kinemetry method.
;
; CATEGORY:
;	Velocity fields fitting.
;
; CALLING SEQUENCE:
;	kinegen,file,error_file=error_file,snr_file=snr_file,plotall=plotall,plotlast=plotlast,verbose=verbose,$
;		name=name,seed=seed,ctrl=ctrl,threading=threading,$
;		seeing=seeing,rwd_index=rwd_index,d0=d0,cover=cover 
;
; INPUTS:
;	FILE:
;		The name of a fits or ad2 file containing the velocity map to be analysed.
;
; OPTIONAL INPUTS:
;	ERROR_FILE:
;		The name of a fits or ad2 file containing errors on velocity measurement. This map should have exactly the same number of pixel
;		with a finite value (ie. different than NaN value) than the velocity map (pointed by the FILE parameter).
;		This keyword should not be set if ERROR_FILE is already set.
;
;	SNR_FILE:
;		A file containing signal to noise for each velocity measurement. This map should have exactly the same number of pixel
;		with a finite value (ie. different than NaN value). This keyword should not be set if ERROR_FILE is already set.
;
;	PARINFO:
;		A structure controlling the value of the four parameters ['PA','INCL','XC','YC']. The structure tags are the following:
;			- PARNAME: the name of the parameter. This name can have only four values which are: 'PA', 'INCL', 'XC', 'YC'
;				If the structure doesn't contain one of those idl strings in the PARNAME tag, the PARINFO keyword will not
;				be taken into account.
;			- LIMITS: a 2 elements array containing the lower limit in parinfo.limits[0] and the upper limit in parinfo.limits[1].
;				Those limits should be strictly be in the interval of the default limits.
;				PA limits: [0,180] degrees
;				INCL limits: [10,85] degrees
;				XC limits: [minimal value of x for finite value pixels,maximal value of x for finite value pixels]
;				YC limits: [minimal value of y for finite value pixels,maximal value of y for finite value pixels]
;			- FIXED: a boolean indicating if the value of the parameter should be fixed during the fitting process. 0=FALSE, 1=TRUE
;			- VALUE: the value of the parameter if the tag FIXED is set to 1. This value should be in the defaults limits.
;
;	PLOTALL:
;		This keyword enable plotting for each kinemetry evaluation. The plot contains the original map, the reconstructed map and the residual map.
;
;	PLOTLAST:
;		This keyword allow plotting just before the end of execution with the final set of parameters
;
;	THREADING:
;		Allow threading implemented in a modified version of PIKAIA. It allows faster computation, with a number of N threads.
;		N is determined thanks to the IDL system constant !CPU.TPOOL_NTHREADS.  
;
;	VERBOSE:
;		Print for each KINEMETRY call the inputs parameters.
;
;	CTRL:
;		PIKAIA CTRL keyword. See PIKAIA routine for documentation.
;
;	RWD_INDEX:
;		A index which control the reward function introduced in the algorithm. This function is necessery to prevent a kinemetry run to 
;		prevent the algorithm to converge towards solutions with a poor number of pixels.
;		The higher the index value is, the higher the fit depends on the monochromatic flux distribution. Inversely, the lower the index value is,
;		the higher the fit depends on the velocity structure.
;		The default value is set to 4.
;
;	SEEING:
;		
; OUTPUTS:
;	params:   store the AMR tree in structure Grid.
;
; COMMON BLOCKS:
;       None.
;
; EXAMPLE:
;       To fit a a velocity field contained in 'vel.fits':
;
;	        kinegen,'vel.fits',error_file='evel.fits',$
;                        ctrl=[300,30,8,.85,2,.20,.20,.75,1.0,3,1,1],seeing=4.8,/threading,/plotall
;
; MODIFICATION HISTORY:
; 	Written by:	Valentin Perret, 01/01/2012.
;                       e-mail: valentin.perret@oamp.fr
;	March, 2012:	Comments and header added by Valentin Perret.
;	April, 2012:
;-
pro kinegen,file,params=params,error_file=error_file,snr_file=snr_file,parinfo=parinfo,$
	plotall=plotall,plotlast=plotlast,verbose=verbose,$
	name=name,seed=seed,ctrl=ctrl,threading=threading,seeing=seeing,$
	rwd_index1=rwd_index1,rwd_index2=rwd_index2,d0=d0,cover=cover,$
	rc_max_vdep=rc_max_vdep,rc_min_slope=rc_min_slope,rc_max_delta=rc_max_delta,rc_max_vr0=rc_max_vr0,$
	mode=mode,nrad=nrad,min_pix=min_pix,max_ell_sampl=max_ell_sampl
        
        print,"KineGen v0.1"
        if(n_params() ne 1) then begin
                doc_library,'kinegen'
                return
        endif
        if(~file_test(file)) then begin
                print,'Velocity field file not found.'
                return
        endif
        file=file_search(file)
        im=readfitsorad(file)
        sizex=(size(im))[1]
        sizey=(size(im))[2]
        imtotabs,im,xbin,ybin,velbin

        if(keyword_set(error_file) and ~keyword_set(snr_file)) then begin
                print,"Locate error field:"
                imerr=readfitsorad(error_file)
                if((size(imerr))[1] ne sizex or (size(imerr))[2] ne sizey) then begin
                	message,"Velocity field and Velocity error field do not have the same dimension"
                endif
                if((where(imerr eq 0.))[0] ne -1) then begin
                	print,"Error field contains values lower or equal to zero. Check the error field."
                	print,"Setting these pixels to the mean value of pixels with error > 0."
                	imerr[where(imerr le 0.)] = mean(imerr[where(imerr gt 0.)],/nan)
                endif
                imtotabs,imerr,xbin,ybin,errbin
        endif else if(keyword_set(snr_file) and ~keyword_set(error_file)) then begin
                print,"Locate snr field:"
                imerr=readfitsorad(snr_file)
                if((size(imerr))[1] ne sizex or (size(imerr))[2] ne sizey) then begin
                	message,"Velocity field and Velocity error field do not have the same dimension"
                endif
                imerr[where(~finite(im))]=!values.f_nan
                ;We need a field proportional to the error on velocity
                imerr=1.0/imerr
                imerr/=max(imerr,/nan)
                imtotabs,imerr,xbin,ybin,errbin
        endif else begin
                errbin = velbin
                errbin[*] = 7   ;km.s
                print,'Setting all errors to 7 km/s'
                ;This is equivalent to say that each pixel have the same weigth in the fitting process
        endelse
        ;If no seeing keyword, we set the value to 1 pixel
        if(n_elements(seeing) eq 0) 		then seeing 		= 1.0
        if(n_elements(d0) eq 0) 		then d0 		= 6.
        if(n_elements(rwd_index1) eq 0) 	then rwd_index1 	= 4.
        if(n_elements(rwd_index2) eq 0) 	then rwd_index2 	= 0.
        if(n_elements(cover) eq 0) 		then cover 		= 0.75
        if(n_elements(rc_max_vdep) eq 0) 	then rc_max_vdep 	= 1000.
        if(n_elements(rc_min_slope) eq 0) 	then rc_min_slope 	= -100.
        if(n_elements(rc_max_delta) eq 0) 	then rc_max_delta 	= 10.
        if(n_elements(rc_max_vr0) eq 0) 	then rc_max_vr0 	= 1000.
        if(n_elements(min_pix) eq 0) 		then min_pix 		= 0.25
        if(n_elements(max_ell_sampl) eq 0) 	then max_ell_sampl 	= 75
        ;We define the parinfo array which contains the limits of the parameters to fit
        ; Array containing the limits of the 4 parameters of the fit
        ; Each parameter is constrained with parinfo[x].limits[0]->min value and parinfo[x].limits[1]->max value

	;Defining a default parinfo structure
        default_parinfo = replicate({VALUE:0.D, FIXED:0, PARNAME:"", LIMITS:[0.D,0.D]}, 4)
        default_parinfo[0].LIMITS       = [0., 180.]
	default_parinfo[0].PARNAME      = "PA"
        default_parinfo[0].FIXED        = 0
        default_parinfo[0].VALUE        = 0
        
        default_parinfo[1].LIMITS       = [10., 85.]
        default_parinfo[1].PARNAME      = "INCL"
        default_parinfo[1].FIXED        = 0
        default_parinfo[1].VALUE        = 0
        
        default_parinfo[2].LIMITS       = [-max(xbin), -min(xbin)]
        default_parinfo[2].PARNAME      = "XC"
        default_parinfo[2].FIXED        = 0
        default_parinfo[2].VALUE        = 0
        
        default_parinfo[3].LIMITS       = [-max(ybin), -min(ybin)]
        default_parinfo[3].PARNAME      = "YC"
        default_parinfo[3].FIXED        = 0
       	default_parinfo[3].VALUE        = 0
	if(keyword_set(parinfo)) then begin
		if(n_elements(tag_names(parinfo)) gt 4) then message,'PARINFO should be a structure with a maximum of 4 elements'
		parinfo_tags = tag_names(parinfo)
		wh = where(parinfo_tags eq 'FIXED',ct)
		if(ct eq 0) then message,'PARINFO should have a FIXED tag'
		wh = where(parinfo_tags eq 'LIMITS',ct)
		if(ct eq 0) then message,'PARINFO should have a LIMITS tag'
		wh = where(parinfo_tags eq 'PARNAME',ct)
		if(ct eq 0) then message,'PARINFO should have a PARNAME tag'
		wh = where(parinfo_tags eq 'VALUE',ct)
		if(ct eq 0) then message,'PARINFO should have a VALUE tag'
		
		wh = where(parinfo.PARNAME eq 'PA',ct)
		if(ct eq 1) then begin 
			if((parinfo[wh[0]].LIMITS)[0] ge (default_parinfo[0].LIMITS)[0] and $
			(parinfo[wh[0]].LIMITS)[1] le (default_parinfo[0].LIMITS)[1] and $
			(parinfo[wh[0]].LIMITS)[0] lt (parinfo[wh[0]].LIMITS)[1] ) then default_parinfo[0].limits = parinfo[wh[0]].LIMITS
			if(parinfo[wh[0]].VALUE ge (default_parinfo[0].LIMITS)[0] and parinfo[wh[0]].VALUE le (default_parinfo[0].LIMITS)[1]) then begin
				default_parinfo[0].VALUE = parinfo[wh[0]].VALUE
				if(parinfo[wh[0]].FIXED eq 1) then default_parinfo[0].FIXED = 1
			endif
		endif
		
		wh = where(parinfo.parname eq 'INCL',ct)
		if(ct eq 1) then begin 
			if((parinfo[wh[0]].LIMITS)[0] ge (default_parinfo[1].LIMITS)[0] and $
			(parinfo[wh[0]].LIMITS)[1] le (default_parinfo[1].LIMITS)[1] and $
			(parinfo[wh[0]].LIMITS)[0] lt (parinfo[wh[0]].LIMITS)[1] ) then default_parinfo[1].limits = parinfo[wh[0]].LIMITS
			if(parinfo[wh[0]].VALUE ge (default_parinfo[1].LIMITS)[0] and parinfo[wh[0]].VALUE le (default_parinfo[1].LIMITS)[1]) then begin
				default_parinfo[1].VALUE = parinfo[wh[0]].VALUE
				if(parinfo[wh[0]].FIXED eq 1) then default_parinfo[1].FIXED = 1
			endif
		endif
		
		wh = where(parinfo.parname eq 'XC',ct)
		if(ct eq 1) then begin 
			if((parinfo[wh[0]].LIMITS)[0] ge (default_parinfo[2].LIMITS)[0] and $
			(parinfo[wh[0]].LIMITS)[1] le (default_parinfo[2].LIMITS)[1] and $
			(parinfo[wh[0]].LIMITS)[0] lt (parinfo[wh[0]].LIMITS)[1] ) then default_parinfo[2].LIMITS = parinfo[wh[0]].LIMITS
			if(parinfo[wh[0]].VALUE ge (default_parinfo[2].LIMITS)[0] and parinfo[wh[0]].VALUE le (default_parinfo[2].LIMITS)[1]) then begin
				default_parinfo[2].VALUE = parinfo[wh[0]].VALUE
				if(parinfo[wh[0]].FIXED eq 1) then default_parinfo[2].FIXED = 1
			endif
		endif
			
		wh = where(parinfo.parname eq 'YC',ct)
		if(ct eq 1) then begin 
			if((parinfo[wh[0]].LIMITS)[0] ge (default_parinfo[3].LIMITS)[0] and $
			(parinfo[wh[0]].LIMITS)[1] le (default_parinfo[3].LIMITS)[1] and $
			(parinfo[wh[0]].LIMITS)[0] lt (parinfo[wh[0]].LIMITS)[1] ) then default_parinfo[3].LIMITS = parinfo[wh[0]].LIMITS
			if(parinfo[wh[0]].VALUE ge (default_parinfo[3].LIMITS)[0] and parinfo[wh[0]].VALUE le (default_parinfo[3].LIMITS)[1]) then begin
				default_parinfo[3].VALUE = parinfo[wh[0]].VALUE
				if(parinfo[wh[0]].FIXED eq 1) then default_parinfo[3].FIXED = 1
			endif
		endif
		if(total(default_parinfo.FIXED) eq 4) then message,'All the parameters are fixed in the PARINFO structure. Check it out!'
	endif

	radius = generate_kinemetry_radii(xbin,ybin,sizex,sizey,seeing,mode=mode,nrad=nrad)
	nrad = n_elements(radius)
	;cvalue=(1.0-exp(-radius/d0))*(exp(1)+((1.0+tanh(10*(seeing/d0-0.5)))/2.0)*(seeing/d0+2.2-exp(1)))
	; This formula has been fitted on 172 velocity fields (exponential disk toymodels) exploring seeing and disk scalelength.
	; This formula insures to minimize the contribution of the PSF in the velocity field of an exponential disk
	var		= seeing/d0
	p0 		= (0.25/var)+1.
	cvalue 		= (1-exp(-p0*radius/d0))*(1.50+0.20*radius/d0)
	
        if(~keyword_set(ctrl)) then ctrl = [200,20,8,.85,2,.20,.20,.75,1.0,3,1,2]
        ; Running Pikaia genetical algorithm
        ; the version used is very close to the original one
        ; We just have changed the arguments, in order to pass the function name,
        ; and extra arguments such as arrays containing the input data for kinemetry
        pikaia,4,ctrl,x,f,status,oldph=oldph,fitns=fitns,fname='pikaia_kinemetry_wrapper',seed=seed,$
                xbin=xbin,ybin=ybin,velbin=velbin,errbin=errbin,parinfo=default_parinfo,$
                plotall=plotall,verbose=verbose,threading=threading,seeing=seeing,$
                rwd_index1=rwd_index1,rwd_index2=rwd_index2,cover=cover,radius=radius,cvalue=cvalue,$
                rc_max_vdep=rc_max_vdep,rc_min_slope=rc_min_slope,rc_max_delta=rc_max_delta,$
                rc_max_vr0=rc_max_vr0,min_pix=min_pix,max_ell_sampl=max_ell_sampl
        ; Printing results
        ; parinfo contains the limits of each parameter
                
        not_fixed = where(default_parinfo.FIXED eq 0,n_notfixed,complement=fixed)
        best_params = fltarr(4)
        if(fixed[0] ne -1) 	then best_params[fixed] = default_parinfo[fixed].VALUE
        if(not_fixed[0] ne -1) 	then best_params[not_fixed] = x*(default_parinfo[not_fixed].limits[1]-default_parinfo[not_fixed].limits[0])$
        	+default_parinfo[not_fixed].limits[0]
        print,"Best parameters"
        pa	= best_params[0]            
        incl	= best_params[1]          
        xc	= best_params[2]           
        yc	= best_params[3]            
        
        err_params = fltarr(4,2)
        if(fixed[0] ne -1) then err_params[fixed,*] = 0
        
        if(n_notfixed gt 1) then begin
        	scaled_oldph = oldph[0:n_notfixed-1,0:ctrl[0]-1]*$
        	transpose(reform(rebin(default_parinfo[not_fixed].limits[1]-default_parinfo[not_fixed].limits[0],n_notfixed*ctrl[0],/sample),ctrl[0],n_notfixed))$
		+transpose(reform(rebin(default_parinfo[not_fixed].limits[0],n_notfixed*ctrl[0],/sample),ctrl[0],n_notfixed))
        endif
        if(n_notfixed eq 1) then begin
        	scaled_oldph = oldph[0:n_notfixed-1,0:ctrl[0]-1]*$
        	replicate(default_parinfo[not_fixed].limits[1]-default_parinfo[not_fixed].limits[0],ctrl[0])$
		+replicate(default_parinfo[not_fixed].limits[0],ctrl[0])
        endif
        ; Here we are taking care of the PA errors estimation
        ; Since the boundaries conditions of this parameter are periodic at the extrema (0° and 180°)
        ; we shift the population PA value towards 90°, in order to have continuous values in the distribution
        if(default_parinfo[where(default_parinfo.parname eq 'PA')].FIXED eq 0) then begin
        	pa_shift = 0
        	oldph_pa_deg_max = max(scaled_oldph[0,*])
        	if(oldph_pa_deg_max gt 175.) then begin
        		pa_shift = -abs(pa-90)
        		scaled_oldph[0,*] += pa_shift
        		ok = where(scaled_oldph[0,*] lt 0.)
        		if(ok[0] ne -1) then scaled_oldph[0,ok] += 180.
        		best_params[0] += pa_shift
        	endif
	endif else begin
		pa_shift = 0
	endelse
        ; The error are evaluated thanks to the farest indiviuals in the residual population
        err_params[not_fixed,0] = max(scaled_oldph,dimension=2)-best_params[not_fixed]
        err_params[not_fixed,1] = abs(min(scaled_oldph,dimension=2)-best_params[not_fixed])        
	
	best_params[0] -= pa_shift
	
	params = fltarr(4,3)
        params[*,0] = best_params
        params[*,1] = err_params[*,0]
        params[*,2] = err_params[*,1]
        
        ; Saving the parameters into a .sav file
        if(~keyword_set(name)) then name=''
        
        ; Re-launching kinemetry with the final parameters
        if(keyword_set(plotlast)) then begin
        	junk=pikaia_kinemetry_wrapper(4,x,parinfo=default_parinfo,xbin=xbin,ybin=ybin,errbin=errbin,velbin=velbin,$
                	velkin=velkin,seeing=seeing,rwd_index1=rwd_index1,rwd_index2=rwd_index2,cover=cover,radius=radius,$
                	cvalue=cvalue,cfvel=cfvel,er_cfvel=er_cfvel,$
                	rc_max_vdep=rc_max_vdep,rc_min_slope=rc_min_slope,rc_max_delta=rc_max_delta,$
                	min_pix=min_pix,rc_max_vr0=rc_max_vr0,max_ell_sampl=max_ell_sampl,/plot)
	endif else begin
		junk=pikaia_kinemetry_wrapper(4,x,parinfo=default_parinfo,xbin=xbin,ybin=ybin,errbin=errbin,velbin=velbin,$
                	velkin=velkin,seeing=seeing,rwd_index1=rwd_index1,rwd_index2=rwd_index2,cover=cover,radius=radius,$
                	cvalue=cvalue,cfvel=cfvel,er_cfvel=er_cfvel,$
                	rc_max_vdep=rc_max_vdep,rc_min_slope=rc_min_slope,rc_max_delta=rc_max_delta,$
                	min_pix=min_pix,rc_max_vr0=rc_max_vr0,max_ell_sampl=max_ell_sampl)
        endelse
        
        ; Writing geometrical parameters into an ASCII file
        openw,lun,strtrim(name,1)+'_kinegen_params.dat',/get_lun
        printf,lun,"PA  ",params[0,0],params[0,1],params[0,2],format='(A,F,F,F)'
        printf,lun,"INCL",params[1,0],params[1,1],params[1,2],format='(A,F,F,F)'
        printf,lun,"XC  ",params[2,0],params[2,1],params[2,2],format='(A,F,F,F)'
        printf,lun,"YC  ",params[3,0],params[3,1],params[3,2],format='(A,F,F,F)'
        close,lun
        if((size(cfvel))[1] gt 0) then begin
        	; Writing kinemetry harmonic coefficients into a FITS structure 
        	cfvel_struct 	= replicate({A0:[0.D,0.D], A1:[0.D,0.D], B1:[0.D,0.D], A2:[0.D,0.D], B2:[0.D,0.D], A3:[0.D,0.D], B3:[0.D,0.D], A4:[0.D,0.D], B4:[0.D,0.D], A5:[0.D,0.D], B5:[0.D,0.D]}, (size(cfvel))[1])
       		cfvel_struct.A0	= transpose([[cfvel[*,0]],[er_cfvel[*,0]]])
        	cfvel_struct.A1	= transpose([[cfvel[*,1]],[er_cfvel[*,1]]])
        	cfvel_struct.B1	= transpose([[cfvel[*,2]],[er_cfvel[*,2]]])
        	cfvel_struct.A2	= transpose([[cfvel[*,3]],[er_cfvel[*,3]]])
        	cfvel_struct.B2	= transpose([[cfvel[*,4]],[er_cfvel[*,4]]])
        	cfvel_struct.A3	= transpose([[cfvel[*,5]],[er_cfvel[*,5]]])
        	cfvel_struct.B3	= transpose([[cfvel[*,6]],[er_cfvel[*,6]]])
        	cfvel_struct.A4	= transpose([[cfvel[*,7]],[er_cfvel[*,7]]])
        	cfvel_struct.B4	= transpose([[cfvel[*,8]],[er_cfvel[*,8]]])
        	cfvel_struct.A5	= transpose([[cfvel[*,9]],[er_cfvel[*,9]]])
        	cfvel_struct.B5	= transpose([[cfvel[*,10]],[er_cfvel[*,10]]])
        	mwrfits,cfvel_struct,strtrim(name,1)+'_kinegen_coeff.fits',/create
        endif
        
        ;Print to screen results
        print,"pa="+strtrim(string(params[0,0],format='(F6.2)'),1)+$
        	" (+"+strtrim(string(params[0,1],format='(F6.2)'),1)+$
        	" -"+strtrim(string(params[0,2],format='(F6.2)'),1)+")"
        
        print,"incl="+strtrim(string(params[1,0],format='(F6.2)'),1)+$
        	" (+"+strtrim(string(params[1,1],format='(F6.2)'),1)+$
        	" -"+strtrim(string(params[1,2],format='(F6.2)'),1)+")"
        print,"In image barycenter coordinates"
        print,"xc="+strtrim(string(params[2,0],format='(F6.2)'),1)+$
        	" (+"+strtrim(string(params[2,1],format='(F6.2)'),1)+$
        	" -"+strtrim(string(params[2,2],format='(F6.2)'),1)+")"
        print,"yc="+strtrim(string(params[3,0],format='(F6.2)'),1)+$
        	" (+"+strtrim(string(params[3,1],format='(F6.2)'),1)+$
        	" -"+strtrim(string(params[3,2],format='(F6.2)'),1)+")"
        print,"In pixel coordinates"
        print,"xc="+strtrim(string((sizex-1)/2.-params[2,0],format='(F6.2)'),1)
        print,"yc="+strtrim(string((sizey-1)/2.-params[3,0],format='(F6.2)'),1)
        if((size(cfvel))[1] gt 0) then print,"vmax="+strtrim(string(max(abs(cfvel[*,2])/sin(params[1,0]*!pi/180.)),format='(F6.2)'),1)
end