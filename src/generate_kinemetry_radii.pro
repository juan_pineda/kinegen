;+
; NAME:
;	GENERATE_KINEMETRY_RADII
;-
function generate_kinemetry_radii,xbin,ybin,sizex,sizey,seeing,mode=mode,verbose=verbose,nrad=nrad
	triangulate,xbin,ybin,tr,vertices
        tr_area=polyfillv(xbin[vertices]+((sizex-1)/2.D),ybin[vertices]+((sizey-1)/2.D),sizex,sizey)
	if(n_elements(nrad) eq 0) then nrad=200
	; The first ellipse should not be too small in order to avoid triangulation problems
	; in the kinemetry routine
	if(mode ne '') then begin
		if(float(n_elements(xbin))/float(n_elements(tr_area)) gt 0.90 and n_elements(xbin) lt 500) then radius=(dindgen(nrad))*0.75+(seeing/6.>0.5) $
		else radius=(dindgen(nrad))^1.75+(seeing/2.>2.0)
		if(strlowcase(mode) eq 'slow') 		then radius=(findgen(nrad))*0.75+(seeing/6.>0.5)
		if(strlowcase(mode) eq 'medium') 	then radius=(findgen(nrad))*1.5+(seeing/6.>0.5)
		if(strlowcase(mode) eq 'fast') 		then radius=(findgen(nrad))^1.75+(seeing/2.>2.0)
		if(strlowcase(mode) eq 'superfast') 	then radius=(findgen(nrad))^2.50+(seeing/2.>2.0)
		if(strlowcase(mode) eq 'autoscale') 	then begin
			rad_index = ((n_elements(xbin)/800.)>1.0)<2.50
			rad_scale = (n_elements(xbin)/800.)
			print,'Rad Index = ',(n_elements(xbin)/800.)
			radius=(dindgen(nrad))^(n_elements(xbin)/800.)+(seeing/6.>0.5)
		endif
	endif else begin
		; Auto-select
		if(float(n_elements(xbin))/float(n_elements(tr_area)) gt 0.90 and n_elements(xbin) lt 500) then begin
			; Case where the density of pixels in the triangulated area is high (high-z galaxies)
			; SLOW MODE
			if(keyword_set(verbose)) then print,"Kinemetry -> Slow Mode"
			radius=(dindgen(nrad))*0.75+(seeing/6.>0.5)
		endif else begin
			; Case where the density of pixels in the triangulated area is low (local galaxies)
			; FAST MODE
			if(keyword_set(verbose)) then print,"Kinemetry -> Fast Mode"
			radius=(dindgen(nrad))^1.75+(seeing/2.>2.0)
			;radius=(dindgen(nrad))^2.0+15
		endelse
	endelse
	return,radius
end