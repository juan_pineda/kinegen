function test_kinemetry,paq,radius,xbin,ybin,velbin,cover=cover,npix=npix

	IF(~KEYWORD_SET(cover)) then cover = 0.75
	ang 	= paq[0]*!pi/180.
        mi 	= (360./(180./10))
	theta 	= range(0.0,2.0*!pi, mi>10*radius[0]<100) 
	x 	= radius[0]*COS(theta)
	y 	= radius[0]*SIN(theta)*paq[1]
	xEll 	= x*COS(ang) - y*SIN(ang)
	yEll 	= x*SIN(ang) + y*COS(ang)
	nbin 	= N_ELEMENTS(xbin)
	IF(nbin NE N_ELEMENTS(ybin) or nbin NE N_ELEMENTS(velbin)) THEN $
		message, 'XBIN, YBIN and VELBIN must have the same size'
	IF nbin LE 1 THEN $
		message, 'XBIN, YBIN should have more than one element'
	nNew = N_ELEMENTS(xell)
	IF nNew NE n_elements(yell) THEN message, 'yell and yell must have the same size'
	
	TRIANGULATE,xbin,ybin,tr,tolerance=(1E-12)
	momNew 	= FLTARR(nNew, /NOZERO)
	FOR j=0,nNew-1 DO momNew[j] = (TRIGRID(xbin, ybin, velbin, tr, $
		XOUT=xell[j]+[0,1], YOUT=yell[j]+[0,1], MISSING=123456789))[0]
		
	w 	= WHERE(momNew NE 123456789, elem)
	npix	= N_ELEMENTS(w)
        IF N_ELEMENTS(w) LT N_ELEMENTS(xEll)*cover THEN return,0
        return,1
        

end